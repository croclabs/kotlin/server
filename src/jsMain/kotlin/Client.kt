
import components.Charts
import kotlinx.browser.document
import react.create
import react.dom.client.createRoot

fun main() {
    val container = document.createElement("div")
    document.body!!.appendChild(container)

    val welcome = Welcome.create {
        name = "Kotlin/JS"
    }

    val charts = Charts.create()

    kotlinext.js.require("./styles/root/Root.sass")
    kotlinext.js.require("./styles/root/App.sass")
    createRoot(container).render(charts)
}