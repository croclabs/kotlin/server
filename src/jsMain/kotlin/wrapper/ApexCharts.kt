@file:JsModule("react-apexcharts")
@file:JsNonModule

package wrapper

import react.ComponentClass
import react.Props

external interface ApexChartsProps : Props {
    var options: dynamic
    var series: dynamic
    var type: String
    var width: String?
    var height: String?
}

@JsName("default")
external val ApexChart : ComponentClass<ApexChartsProps>