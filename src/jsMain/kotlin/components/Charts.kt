package components

import react.FC
import react.Props
import react.dom.html.ReactHTML.div
import wrapper.ApexChart

val Charts = FC<Props> {
    div {
        ApexChart {
            options = js("""
             {
                chart: {
                  id: "basic-bar"
                },
                xaxis: {
                  categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1999]
                }
              }
            """)
            series = js("""
             [
                {
                  name: "Poulation",
                  data: [30, 40, 45, 50, 49, 60, 70, 90]
                }
              ]
            """)
            type = "bar"
            width = "500"
        }
        ApexChart {
            options = js(
                """
                {
                    labels: ["A", "B", "C", "D", "E"]
                }
                """
            )
            series = js("""[100, 55, 41, 17, 15]""")
            type = "donut"
            width = "500"
        }
    }
}