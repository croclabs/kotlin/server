package com.gitlab.croclabs.application

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
class ApplicationController {
    @GetMapping
    fun index(): String {
        return "/index.html"
    }

    @GetMapping("/data")
    @ResponseBody
    fun test(): Any? {
        return mapOf(
            Pair(1, 2),
            Pair(2, 3)
        )
    }
}